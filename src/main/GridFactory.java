package main;

/**
 * Created by Michal Dvořáček on 12.11.16.
 */

import com.jogamp.opengl.GL4;
import oglutils.OGLBuffers;
import oglutils.ToFloatArray;
import oglutils.ToIntArray;
import transforms.Vec2D;

import java.util.ArrayList;
import java.util.List;

public class GridFactory {
    private GL4 gl;

    List<Vec2D> vertexData = new ArrayList<>();
    List<Integer> indexData = new ArrayList<>();

    int m, n;

    public GridFactory(int m, int n, GL4 gl) {
        this.gl = gl;
        this.m = m;
        this.n = n;

        for (int i = 0; i < m; i++)
            for (int j = 0; j < n; j++)
                vertexData.add(new Vec2D((double) j / (n - 1), (double) i / (m - 1)));
        for (int i = 0; i < m - 1; i++)
            for (int j = 0; j < n - 1; j++) {
                indexData.add(i * n + j);
                indexData.add(i * n + j + 1);
                indexData.add((i + 1) * n + j);
                indexData.add(i * n + j + 1);
                indexData.add((i + 1) * n + j);
                indexData.add((i + 1) * n + j + 1);
            }
    }

    public OGLBuffers getBuffers() {
        OGLBuffers.Attrib[] attributes = {
                new OGLBuffers.Attrib("inPosition", 2)};
        return new OGLBuffers(gl, ToFloatArray.convert(vertexData), attributes, ToIntArray.convert(indexData));
    }
}