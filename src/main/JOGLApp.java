package main;

import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.util.FPSAnimator;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class JOGLApp {
    private static final int FPS = 60; // animator's target frames per second

    public static void main(String[] args) {
        try {


            // setup OpenGL Version 4
            GLProfile profile = GLProfile.get(GLProfile.GL4);
            GLCapabilities capabilities = new GLCapabilities(profile);

            // setup frame
            JFrame testFrame = new JFrame("TestFrame");
            GridBagConstraints gridCon = new GridBagConstraints();
            testFrame.setSize(700, 384);
            testFrame.setLayout(new GridBagLayout());

            Container setupContainer = new Container();

            JLabel labelObjects = new JLabel("Switch between objects");

            String[] objects = {"Sombrero", "Vase", "Elephant head", "func", "Mobius band", "Sphere" };
            JComboBox comboBoxObjects = new JComboBox<String>();

            for (String s : objects) {
                comboBoxObjects.addItem(s);
            }

            JLabel labelSurface = new JLabel("Switch between types of surface");

            String[] surfaces = {"Debug", "Normal debug", "Texture", "Normal mapping", "Texture mapping"};
            JComboBox comboBoxSurfaces = new JComboBox<String>();

            for (String s : surfaces) {
                comboBoxSurfaces.addItem(s);
            }

            setupContainer.add(labelSurface);
            setupContainer.add(comboBoxSurfaces);

            setupContainer.add(labelObjects);
            setupContainer.add(comboBoxObjects);

            JButton resetPosButton = new JButton("Reset position");
            setupContainer.add(resetPosButton);

            setupContainer.setLayout(new FlowLayout());
            gridCon.gridx = 0;
            gridCon.gridy = 0;

            testFrame.add(setupContainer, gridCon);

            // The canvas is the widget that's drawn in the JFrame
            GLCanvas canvas = new GLCanvas(capabilities);
            Renderer ren = new Renderer();
            canvas.addGLEventListener(ren);
            canvas.addMouseListener(ren);
            canvas.addMouseMotionListener(ren);
            canvas.addKeyListener(ren);
            canvas.setSize( 512, 384 );

            gridCon.fill = GridBagConstraints.BOTH;
            gridCon.gridx = 0;
            gridCon.gridy = 1;
            gridCon.weightx = 1;
            gridCon.weighty = 1;

            testFrame.add(canvas, gridCon);

            comboBoxObjects.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    ren.setObjectShape(comboBoxObjects.getSelectedIndex());
                    System.out.println(comboBoxObjects.getSelectedIndex());
                }
            });

            comboBoxSurfaces.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    ren.setSurface(comboBoxSurfaces.getSelectedIndex());
                    System.out.println(comboBoxSurfaces.getSelectedIndex());
                }
            });

            resetPosButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    ren.resetCam();
                }
            });


            // shutdown the program on windows close event

            //final Animator animator = new Animator(canvas);
            final FPSAnimator animator = new FPSAnimator(canvas, FPS, true);

            testFrame.addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosing(WindowEvent e) {
                    new Thread() {
                        @Override
                        public void run() {
                            if (animator.isStarted()) animator.stop();
                            System.exit(0);
                        }
                    }.start();
                }
            });
            //testFrame.setTitle("");
            testFrame.pack();
            testFrame.setVisible(true);
            animator.start(); // start the animation loop


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}