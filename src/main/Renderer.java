package main;

import com.jogamp.opengl.*;

import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseEvent;
import java.awt.event.KeyListener;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;

import com.jogamp.opengl.util.texture.Texture;
import com.jogamp.opengl.util.texture.TextureIO;
import oglutils.OGLUtils;
import oglutils.ShaderUtils;
import oglutils.ToFloatArray;
import transforms.Camera;
import transforms.Mat4;
import transforms.Mat4PerspRH;
import transforms.Vec3D;

/**
 * Ukazka pro praci s shadery v GLSL
 * nacteni souboru s texturou
 * upraveno pro JOGL 2.3.0 a vyssi
 *
 * @author PGRF FIM UHK
 * @version 2.0
 * @since   2015-09-05
 */

public class Renderer implements GLEventListener, MouseListener,
        MouseMotionListener, KeyListener {

    int width, height, ox, oy;

    private GridFactory gf;

    int shaderProgram, locMat;

    Texture texture, textureN;

    Camera cam = new Camera();
    Mat4 proj;

    private int objectShape, choosenObject, surface, choosenSurface, camera;

    public void init(GLAutoDrawable glDrawable) {
        GL4 gl = glDrawable.getGL().getGL4();

        OGLUtils.printOGLparameters(gl);

        shaderProgram = ShaderUtils.loadProgram(gl, "/shaders/texture");

        gf =new GridFactory(50, 50, gl);

        locMat = gl.glGetUniformLocation(shaderProgram, "mat");
        choosenObject = gl.glGetUniformLocation(shaderProgram, "objectShape");
        choosenSurface = gl.glGetUniformLocation(shaderProgram, "surface");
        camera = gl.glGetUniformLocation(shaderProgram, "camera");

        cam = cam.withPosition(new Vec3D(5, 5, 2.5))
                .withAzimuth(Math.PI * 1.25)
                .withZenith(Math.PI * -0.125);

        gl.glEnable(GL4.GL_DEPTH_TEST);

        try {
            texture = TextureIO.newTexture(new File("res/textures/bricks.jpg"), false);
            textureN = TextureIO.newTexture(new File("res/textures/bricksn.png"), false);
        } catch (IOException e) {
            e.printStackTrace();
        }


    }


    public void display(GLAutoDrawable glDrawable) {
        GL4 gl = glDrawable.getGL().getGL4();

        gl.glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
        gl.glClear(GL4.GL_COLOR_BUFFER_BIT | GL4.GL_DEPTH_BUFFER_BIT);

        gl.glUseProgram(shaderProgram);
        gl.glUniformMatrix4fv(locMat, 1, false,
                ToFloatArray.convert(cam.getViewMatrix().mul(proj)), 0);

        gl.glActiveTexture(GL4.GL_TEXTURE);
        texture.bind(gl);
        texture.setTexParameteri(gl,GL4.GL_TEXTURE_WRAP_S,GL4.GL_REPEAT);
        texture.setTexParameteri(gl,GL4.GL_TEXTURE_WRAP_T,GL4.GL_REPEAT);
        int locTexture = gl.glGetUniformLocation(shaderProgram, "texture");
        gl.glUniform1i(locTexture, 0);

        gl.glActiveTexture(GL4.GL_TEXTURE1);
        textureN.bind(gl);
        int locTextureN = gl.glGetUniformLocation(shaderProgram, "textureN");
        gl.glUniform1i(locTextureN, 1);

        gl.glUniform3f(camera,(float) cam.getPosition().getX(),(float) cam.getPosition().getY(),(float) cam.getPosition().getZ());

        gl.glUniform1i(choosenObject, objectShape);
        gl.glUniform1i(choosenSurface, surface);


        gf.getBuffers().draw(GL4.GL_TRIANGLES, shaderProgram);
    }

    public void reshape(GLAutoDrawable drawable, int x, int y, int width,
                        int height) {
        this.width = width;
        this.height = height;
        proj = new Mat4PerspRH(Math.PI / 4, height / (double) width, 0.01, 1000.0);
    }

    public void mouseClicked(MouseEvent e) {
    }

    public void mouseEntered(MouseEvent e) {
    }

    public void mouseExited(MouseEvent e) {
    }

    public void mousePressed(MouseEvent e) {
        ox = e.getX();
        oy = e.getY();
    }

    public void mouseReleased(MouseEvent e) {
    }

    public void mouseDragged(MouseEvent e) {
        cam = cam.addAzimuth((double) Math.PI * (ox - e.getX()) / width)
                .addZenith((double) Math.PI * (e.getY() - oy) / width);
        ox = e.getX();
        oy = e.getY();
    }

    public void mouseMoved(MouseEvent e) {
    }

    public void keyPressed(KeyEvent e) {
        switch (e.getKeyCode()) {
            case KeyEvent.VK_W:
                cam = cam.forward(1);
                break;
            case KeyEvent.VK_D:
                cam = cam.right(1);
                break;
            case KeyEvent.VK_S:
                cam = cam.backward(1);
                break;
            case KeyEvent.VK_A:
                cam = cam.left(1);
                break;
            case KeyEvent.VK_CONTROL:
                cam = cam.down(1);
                break;
            case KeyEvent.VK_SHIFT:
                cam = cam.up(1);
                break;
            case KeyEvent.VK_SPACE:
                cam = cam.withFirstPerson(!cam.getFirstPerson());
                break;
            case KeyEvent.VK_R:
                cam = cam.mulRadius(0.9f);
                break;
            case KeyEvent.VK_F:
                cam = cam.mulRadius(1.1f);
                break;
        }
    }

    public void keyReleased(KeyEvent e) {
    }

    public void keyTyped(KeyEvent e) {
    }

    public void dispose(GLAutoDrawable glDrawable) {
        GL2 gl = glDrawable.getGL().getGL2();
        gl.glDeleteProgram(shaderProgram);
    }

    public int getObjectShape() {
        return objectShape;
    }

    public void setObjectShape(int objectShape) {
        this.objectShape = objectShape;
    }

    public int getSurface() {
        return surface;
    }

    public void setSurface(int surface) {
        this.surface = surface;
    }

    public void resetCam(){
        cam = cam.withPosition(new Vec3D(5, 5, 2.5))
                .withAzimuth(Math.PI * 1.25)
                .withZenith(Math.PI * -0.125);
    }
}