#version 330
in vec2 position;
in vec3 vertColor;
in vec2 texCoord;
in vec3 normal;

in vec3 viewDirection;
in vec3 lightDirection;
in float lightDistance;
in vec3 lightTBN;

out vec4 outColor;

uniform sampler2D texture;
uniform sampler2D textureN;
uniform int surface;

vec3 baseColor;
vec3 lightd;
vec3 viewd;
vec3 bump;

float att= 1.0 / (0.158 + 0.035 * lightDistance + 0.00015 * lightDistance * lightDistance);

void perPixel(){
    vec3 ambient = vec3(0.1, 0.1, 0.1);
    float dotLight = max(dot(normalize(normal), lightd),0.0);
    if(surface==3){
        dotLight = max(dot(bump, lightd),0.0);
    }
    vec3 diffuse = att * dotLight * vec3(0.25, 0.25, 0.25);
    vec3 halfv = lightd + viewd;
    halfv = normalize(halfv);
    float dotHalf = max(dot(normalize(normal), halfv), 0.0);
    if (surface == 3){
        dotHalf= max(dot(bump,halfv), 0.0);
    }
    vec3 specular =  att * pow(dotHalf,30) * vec3(0.2, 0.2, 0.2);
    vec3 color = baseColor * ( ambient + diffuse) + specular;
    outColor = vec4(color.xyz, 1.0);
}

void chooseSurface(){
    if(surface == 0){
        outColor = vec4(normalize(position),0,1);
    }
    else if(surface == 1){
        outColor = vec4(normalize(normal),1);
    }
    else if(surface == 2){
         outColor = texture(texture, texCoord);
         perPixel();
     }
    else if (surface == 3){
         outColor = vec4(vertColor,1.0);
    	 perPixel();
    }
}

void main() {
    lightd = normalize(lightDirection);
    viewd = normalize(viewDirection);
    outColor = texture(texture, vec2(2 * texCoord.x - floor(2 * texCoord.x),2 * texCoord.y - floor(2 * texCoord.y)));
    baseColor = outColor.xyz;

    if (surface == 3){
        bump = texture(textureN, vec2(2 * texCoord.x - floor(2 * texCoord.x),2 * texCoord.y - floor(2 * texCoord.y))).rgb * 2.0 - 1.0;
        bump = normalize(bump);
    }
    chooseSurface();
} 
