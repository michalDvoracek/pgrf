#version 330
in vec2 inPosition;

out vec3 vertColor;
out vec2 texCoord;
out vec3 normal;
out vec2 position;

uniform mat4 mat;
uniform int objectShape;
uniform vec3 camera;
uniform int surface;

const float PI = 3.1415927;
const float DELTA = 0.001;

vec3 newPosition;
out vec3 viewDirection;
out vec3 lightDirection;
out float lightDistance;
out vec3 lightTBN;

vec3 sombrero(vec2 inPosition){
	vec2 position = inPosition;
	float s = 3.9 - PI * position.x * 2;
	float t = 2 * PI * position.y;
	vec3 newPosition =  vec3(t*cos(s),t*sin(s),2*sin(t))/2;
	return newPosition;
}

vec3 vase(vec2 inPosition){
	vec2 position = inPosition;
	float s = 4 -  2 * PI * position.x;
	float t = 2 * PI * position.y;
	vec3 newPosition = vec3((2+cos(t))/(3+sin(t))*cos(s),(2+cos(t))/(3+sin(t))*sin(s),2-t)/1.5;
	return newPosition;
}

vec3 elephant(vec2 inPosition) {
	vec2 position = inPosition;
	float azimuth = position.x * 2.0 * PI;
	float zenith = position.y * PI - PI / 2;
	float R = 8.0 + cos(4 * azimuth);
	vec3 newPosition = vec3(R * cos(zenith) * cos(azimuth),R * cos(zenith) * sin(azimuth),R * sin(zenith))/4;
	return newPosition;
}

vec3 carpet(vec2 inPosition){
	vec2 position = inPosition;
	float azimuth = position.x * 2.0 * PI;
	float zenith = position.y * PI - PI / 2;
	vec3 newPosition;
	newPosition.xy= (position - 0.3)*5;
	newPosition.z= (4 * (cos(sqrt(10 * (position.x * position.x)+3 * (position.y * position.y)+5))-0.5))+6;
	return newPosition;
}

vec3 mobius(vec2 inPosition){
	vec2 position = inPosition;
	float azimuth = position.x * 2.0 * PI;
	float zenith = position.y - 0.5;
	vec3 newPosition;
	newPosition.x = 2 * cos(azimuth) + zenith * cos(azimuth/2);
	newPosition.y = 2 * sin(azimuth) + zenith * cos(azimuth/2);
	newPosition.z = (zenith * sin(azimuth/2));
	return newPosition;
}

vec3 sphere(vec2 inPosition){
    vec2 position = inPosition;
    float t = 2* PI * position.y;
    float s = PI * 0.5 - PI * inPosition.x;
    float r = 2;
    vec3 newPosition = vec3(cos(t) * cos(s) * r,sin(t) * cos(s) * r,sin(s) * r);
    return newPosition;
}



vec3 chooseShape(vec2 inPosition){
    if(objectShape == 0)
        newPosition = sombrero(inPosition);
    if(objectShape == 1)
        newPosition = vase(inPosition);
    if(objectShape == 2)
        newPosition = elephant(inPosition);
    if(objectShape == 3)
        newPosition = carpet(inPosition);
    if(objectShape == 4)
        newPosition = mobius(inPosition);
    if(objectShape == 5)
        newPosition = sphere(inPosition);
    return newPosition;
}

vec3 calculateNormal (vec2 uv) {
	vec3 x = (chooseShape(uv+vec2(DELTA,0))-chooseShape(uv-vec2(DELTA,0)))/2.0/DELTA;
	vec3 y = (chooseShape(uv+vec2(0,DELTA))-chooseShape(uv-vec2(0,DELTA)))/2.0/DELTA;
	return cross(x,y);
}

vec3 calculateTangent (vec2 uv) {
	vec3 x = (chooseShape(uv+vec2(DELTA,0))-chooseShape(uv-vec2(DELTA, 0)))/2.0/DELTA;
	return x;
}

void perVertex(){
    vec3 lightd = lightDirection;
    vec3 viewd = viewDirection;
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
    vec3 baseColor = vec3(0.5, 0.2, 0.7);
    ambient = vec3(0.1, 0.1, 0.1);
    ambient = baseColor * ambient;
    float dotLight = max(dot(normalize(normal), normalize(lightd)),0.0);
    diffuse = dotLight * vec3(0.25, 0.25, 0.25);
    diffuse = baseColor * diffuse;
    vec3 halfv = lightd + viewd;
    halfv = normalize(halfv);
    float dotHalf= max(dot(normalize(normal), halfv), 0.0);
    specular = pow(dotHalf,50) * vec3(0.8, 0.8, 0.8);
    vertColor = ambient + diffuse + specular;
}

void main() {
    newPosition = chooseShape(inPosition);
    normal = calculateNormal(inPosition);

	gl_Position = mat * vec4(newPosition, 1.0);

    vec3 light = camera;
	//vec3 light = vec3(5,5,2);
    lightDirection = light - newPosition;
    viewDirection = normalize(camera);
    lightDistance = length(newPosition - camera);
    if (surface == 1){
    vec3 tan = calculateTangent(inPosition);
    tan = normalize(tan);
    vec3 bitan = cross(normalize(normal),tan);
    bitan = normalize(bitan);
    mat3 TBN = mat3(tan,bitan,normalize(normal));
    viewDirection = normalize(viewDirection * TBN);
    lightTBN = normalize(lightDirection * TBN);
    }
    perVertex();

	texCoord = inPosition.xy * 4;
    position = inPosition;
}
